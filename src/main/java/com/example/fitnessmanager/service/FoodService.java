package com.example.fitnessmanager.service;

import com.example.fitnessmanager.dto.food.FoodCreateDto;
import com.example.fitnessmanager.dto.food.FoodFullDto;
import com.example.fitnessmanager.dto.food.FoodUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FoodService {

    FoodFullDto findById(Long id);

    List<FoodFullDto> findAll();

    FoodFullDto create(FoodCreateDto foodCreateDto);

    FoodFullDto update(FoodUpdateDto foodUpdateDto);

    void deleteById(Long id);
}
