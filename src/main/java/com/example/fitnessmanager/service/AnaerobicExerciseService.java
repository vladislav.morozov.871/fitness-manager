package com.example.fitnessmanager.service;

import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseCreateDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseFullDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AnaerobicExerciseService {

    AnaerobicExerciseFullDto findById(Long id);

    List<AnaerobicExerciseFullDto> findAll();

    AnaerobicExerciseFullDto create(AnaerobicExerciseCreateDto anaerobicExerciseCreateDto);

    AnaerobicExerciseFullDto update(AnaerobicExerciseUpdateDto anaerobicExerciseUpdateDto);

    void deleteById(Long id);
}
