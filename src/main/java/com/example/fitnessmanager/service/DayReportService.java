package com.example.fitnessmanager.service;

import com.example.fitnessmanager.dto.dayReport.DayReportFullDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DayReportService {

    DayReportFullDto findById(Long id);

    List<DayReportFullDto> findAll();

    void deleteById(Long id);
}
