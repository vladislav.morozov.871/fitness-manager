package com.example.fitnessmanager.service.impl;

import com.example.fitnessmanager.dto.dayReport.DayReportFullDto;
import com.example.fitnessmanager.entity.DayReportEntity;
import com.example.fitnessmanager.exeption.DayReportNotNullFieldExeption;
import com.example.fitnessmanager.exeption.EntityNotFoundExeption;
import com.example.fitnessmanager.repository.DayReportRepository;
import com.example.fitnessmanager.service.DayReportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.example.fitnessmanager.mapper.DayReportMapper.DAY_REPORT_MAPPER;

@Slf4j
@Service
public class DayReportServiceImpl implements DayReportService {

    @Autowired
    private DayReportRepository dayReportRepository;


    @Override
    @Transactional(readOnly = true)
    public DayReportFullDto findById(Long id) {
        DayReportEntity foundEntity = dayReportRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        log.info("DayReportEntityServiceImpl -> foud DayReportEntity {} by id: {}", foundEntity, id);

        return DAY_REPORT_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DayReportFullDto> findAll() {
        List<DayReportEntity> foundEntities = dayReportRepository.findAll();

        log.info("DayReportEntityServiceImpl -> found {} DayReportEntity", foundEntities.size());

        return DAY_REPORT_MAPPER.toFullDtoList(foundEntities);
    }


    @Override
    public void deleteById(Long id) {
        dayReportRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        dayReportRepository.deleteById(id);

        log.info("DayReportEntityServiceImpl -> DayReportEntity {} successfully deleted", id);
    }

//    @Transactional
//    public void checkIfUserOrSumConsumedCaloriesOrSumBurnedCaloriesIsTaken(DayReportEntity entity) {
//        List<DayReportEntity> foudDayReports = dayReportRepository.findByUserEntityOrSumConsumedCaloriesOrSumBurnedCalories(entity.getUser(),
//                entity.getSumConsumedCalories(),
//                entity.getSumBurnedCalories());
//
//        foudDayReports.stream()
//                .filter(dayReportEntity -> !dayReportEntity.getUser()
//                        .equals(dayReportEntity
//                                .getSumConsumedCalories()
//                                .equals(dayReportEntity.getSumConsumedCalories())))
//                .forEach(dayReportEntity -> {
//                    throw new DayReportNotNullFieldExeption("User:" + entity.getUser()
//                            + "SumBurnedCalories:" + entity.getSumBurnedCalories()
//                            + "SumConsumedCalories:" + entity.getSumConsumedCalories());
//                });
//    }
}
