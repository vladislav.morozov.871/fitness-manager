package com.example.fitnessmanager.service.impl;

import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseCreateDto;
import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseFullDto;
import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseUpdateDto;
import com.example.fitnessmanager.entity.*;
import com.example.fitnessmanager.exeption.*;
import com.example.fitnessmanager.repository.AerobicExerciseRepository;
import com.example.fitnessmanager.repository.DayReportRepository;
import com.example.fitnessmanager.repository.UserRepository;
import com.example.fitnessmanager.service.AerobicExerciseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.example.fitnessmanager.mapper.AerobicExerciseMapper.AEROBIC_EXERCISE_MAPPER;

@Slf4j
@Service
public class AerobicExerciseServiceImpl implements AerobicExerciseService {

    @Autowired
    private AerobicExerciseRepository aerobicExerciseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DayReportRepository dayReportRepository;

    @Override
    @Transactional(readOnly = true)
    public AerobicExerciseFullDto findById(Long id) {
        AerobicExerciseEntity foundEntity = aerobicExerciseRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        log.info("AerobicExerciseEntityServiceImpl -> found AerobicExerciseEntity {} by id: {}", foundEntity, id);

        return AEROBIC_EXERCISE_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AerobicExerciseFullDto> findAll() {
        List<AerobicExerciseEntity> foundEntities = aerobicExerciseRepository.findAll();

        log.info("AerobicExerciseEntityServiceImpl -> found {} AerobicExerciseEntity", foundEntities.size());

        return AEROBIC_EXERCISE_MAPPER.toFullDtoList(foundEntities);
    }

    @Override
    public AerobicExerciseFullDto create(AerobicExerciseCreateDto aerobicExerciseCreateDto) {
        AerobicExerciseEntity entityToSave = AEROBIC_EXERCISE_MAPPER.mapToEntity(aerobicExerciseCreateDto);

        UserEntity userEntity = userRepository.findById(aerobicExerciseCreateDto.getUserId())
                .orElseThrow(() -> new EntityNotFoundExeption(entityToSave.getId()));

        DayReportEntity dayReportEntity = dayReportRepository.findById(aerobicExerciseCreateDto.getDayReportId())
                .orElseThrow(() -> new EntityNotFoundExeption(entityToSave.getId()));

        entityToSave.setUser(userEntity);
        entityToSave.setDayReport(dayReportEntity);

//        checkIfUserOrCaloriesPerMinuteOrNameExist(entityToSave);

        AerobicExerciseEntity savedEntity = aerobicExerciseRepository.save(entityToSave);

        log.info("AerobicExerciseEntityServiceImpl -> AerobicExerciseEntity {} successfully saved", entityToSave);

        return AEROBIC_EXERCISE_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public AerobicExerciseFullDto update(AerobicExerciseUpdateDto aerobicExerciseUpdateDto) {
        AerobicExerciseEntity entityToUpdate = aerobicExerciseRepository.findById(aerobicExerciseUpdateDto.getId())
                .orElseThrow(() -> new EntityNotFoundExeption(aerobicExerciseUpdateDto.getId()));

        entityToUpdate.setCaloriesPerMinute(aerobicExerciseUpdateDto.getCaloriesPerMinute());
        entityToUpdate.setLeadTime(aerobicExerciseUpdateDto.getLeadTime());

        AerobicExerciseEntity updatedEntity = aerobicExerciseRepository.save(entityToUpdate);

        log.info("AerobicExerciseEntityServiceImpl -> AerobicExerciseEntity {} successfully updated", updatedEntity);

        return AEROBIC_EXERCISE_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    public void deleteById(Long id) {
        aerobicExerciseRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        aerobicExerciseRepository.deleteById(id);

        log.info("AerobicExerciseEntityServiceImpl -> AerobicExerciseEntity {} successfully deleted", id);
    }

//    @Transactional
//    public void checkIfUserOrCaloriesPerMinuteOrNameExist(AerobicExerciseEntity entity) {
//        List<AerobicExerciseEntity> foundAerobicExercises = aerobicExerciseRepository.findByUserEntityOrCaloriesPerMinuteOrName(entity.getUser(),
//                entity.getCaloriesPerMinute(),
//                entity.getName());
//
//        foundAerobicExercises.stream()
//                .filter(aerobicExerciseEntity -> !aerobicExerciseEntity.getUser()
//                        .equals(aerobicExerciseEntity.getCaloriesPerMinute()
//                                .equals(aerobicExerciseEntity.getName())))
//                .forEach(aerobicExerciseEntity -> {
//                    throw new AerobicExerciseNotNullFieldExeption("User:" + entity.getUser()
//                            + "CaloriesPerMinute:" + entity.getCaloriesPerMinute()
//                            + "Name:" + entity.getName());
//                });
//    }
}
