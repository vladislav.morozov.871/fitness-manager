package com.example.fitnessmanager.service.impl;

import com.example.fitnessmanager.dto.user.UserCreateDto;
import com.example.fitnessmanager.dto.user.UserFullDto;
import com.example.fitnessmanager.dto.user.UserUpdateDto;
import com.example.fitnessmanager.entity.UserEntity;
import com.example.fitnessmanager.exeption.EntityNotFoundExeption;
import com.example.fitnessmanager.exeption.UserNotNullFieldExeption;
import com.example.fitnessmanager.repository.UserRepository;
import com.example.fitnessmanager.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.example.fitnessmanager.mapper.UserMapper.USER_MAPPER;

import java.util.List;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserFullDto findById(Long id) {
        UserEntity foundEntity = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        log.info("UserServiceImpl -> foud UserEntity {} by id: {}", foundEntity, id);

        return USER_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserFullDto> findAll() {
        List<UserEntity> foundEntities = userRepository.findAll();

        log.info("UserServiceImpl -> found {} UserEntity", foundEntities.size());

        return USER_MAPPER.toFullDtoList(foundEntities);
    }

    @Override
    @Transactional
    public UserFullDto create(UserCreateDto userCreateDto) {
        UserEntity entityToSave = USER_MAPPER.mapToEntity(userCreateDto);

//        checkIfUserNameOrEmailIsTakenExist(entityToSave);
        entityToSave.setUserName("zxc");

        UserEntity savedEntity = userRepository.save(entityToSave);

        log.info("UserServiceImpl -> UserEntity {} successfully saved", savedEntity);

        return USER_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto userUpdateDto) {
        UserEntity entityToUpdate = userRepository.findById(userUpdateDto.getId())
                .orElseThrow(() -> new EntityNotFoundExeption(userUpdateDto.getId()));

        entityToUpdate.setAge(userUpdateDto.getAge());

        UserEntity updatedEntity = userRepository.save(entityToUpdate);

        log.info("UserServiceImpl -> UserEntity {} successfully updated", updatedEntity);
        return USER_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        userRepository.deleteById(id);

        log.info("UserServiceImpl -> UserEntity {} successfully deleted", id);
    }

//    @Transactional
//    public void checkIfUserNameOrEmailIsTakenExist(UserEntity entity) {
//        List<UserEntity> foundUsers = userRepository.findByUserNameOrEmail(entity.getUserName(), entity.getEmail());
//
//        foundUsers.stream()
//                .filter(userEntity -> !userEntity.getUserName().equals(userEntity.getEmail()))
//                .forEach(userEntity -> {
//                    throw new UserNotNullFieldExeption(
//                            "UserName:" + entity.getUserName() + "Email:" + entity.getEmail());
//                });
//    }
}
