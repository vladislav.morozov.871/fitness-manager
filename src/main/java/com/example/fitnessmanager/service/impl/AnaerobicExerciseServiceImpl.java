package com.example.fitnessmanager.service.impl;

import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseCreateDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseFullDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseUpdateDto;
import com.example.fitnessmanager.entity.*;
import com.example.fitnessmanager.exeption.AnaerobicExerciseNotNullFieldExeption;
import com.example.fitnessmanager.repository.AnaerobicExerciseRepository;
import com.example.fitnessmanager.repository.DayReportRepository;
import com.example.fitnessmanager.exeption.EntityNotFoundExeption;
import com.example.fitnessmanager.repository.UserRepository;
import com.example.fitnessmanager.service.AnaerobicExerciseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.example.fitnessmanager.mapper.AnaerobicExerciseMapper.ANAEROBIC_EXERCISE_MAPPER;

@Slf4j
@Service
public class AnaerobicExerciseServiceImpl implements AnaerobicExerciseService {

    @Autowired
    private AnaerobicExerciseRepository anaerobicExerciseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DayReportRepository dayReportRepository;

    @Override
    @Transactional(readOnly = true)
    public AnaerobicExerciseFullDto findById(Long id) {
        AnaerobicExerciseEntity foundEntity = anaerobicExerciseRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        log.info("AnaerobicExerciseEntityServiceImpl -> found AnaerobicExerciseEntity {} by id: {}", foundEntity, id);

        return ANAEROBIC_EXERCISE_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AnaerobicExerciseFullDto> findAll() {
        List<AnaerobicExerciseEntity> foundEntities = anaerobicExerciseRepository.findAll();

        log.info("AnaerobicExerciseEntityServiceImpl -> found {} AnaerobicExerciseEntity", foundEntities.size());

        return ANAEROBIC_EXERCISE_MAPPER.toFullDtoList(foundEntities);
    }

    @Override
    public AnaerobicExerciseFullDto create(AnaerobicExerciseCreateDto anaerobicExerciseCreateDto) {
        AnaerobicExerciseEntity entityToSave = ANAEROBIC_EXERCISE_MAPPER.mapToEntity(anaerobicExerciseCreateDto);

        UserEntity userEntity = userRepository.findById(anaerobicExerciseCreateDto.getUserId())
                .orElseThrow(() -> new EntityNotFoundExeption(entityToSave.getId()));

        DayReportEntity dayReportEntity = dayReportRepository.findById(anaerobicExerciseCreateDto.getDayReportId())
                .orElseThrow(() -> new EntityNotFoundExeption(entityToSave.getId()));

//        checkIfUserOrApproachQuantityOrNameExist(entityToSave);

        entityToSave.setUser(userEntity);
        entityToSave.setDayReport(dayReportEntity);

        AnaerobicExerciseEntity savedEntity = anaerobicExerciseRepository.save(entityToSave);

        log.info("AnaerobicExerciseEntityServiceImpl -> AnaerobicExerciseEntity {} successfully saved", entityToSave);

        return ANAEROBIC_EXERCISE_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public AnaerobicExerciseFullDto update(AnaerobicExerciseUpdateDto anaerobicExerciseUpdateDto) {
        AnaerobicExerciseEntity entityToUpdate = anaerobicExerciseRepository.findById(anaerobicExerciseUpdateDto.getId())
                .orElseThrow(() -> new EntityNotFoundExeption(anaerobicExerciseUpdateDto.getId()));

        entityToUpdate.setApproachQuantity(anaerobicExerciseUpdateDto.getApproachQuantity());
        entityToUpdate.setRepetitionsApproachQuantity(anaerobicExerciseUpdateDto.getRepetitionsApproachQuantity());

        AnaerobicExerciseEntity updatedEntity = anaerobicExerciseRepository.save(entityToUpdate);

        log.info("AnaerobicExerciseEntityServiceImpl -> AnaerobicExerciseEntity {} successfully updated", updatedEntity);

        return ANAEROBIC_EXERCISE_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    public void deleteById(Long id) {
        anaerobicExerciseRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        anaerobicExerciseRepository.deleteById(id);

        log.info("AnaerobicExerciseEntityServiceImpl -> AnaerobicExerciseEntity {} successfully deleted", id);
    }

//    @Transactional
//    public void checkIfUserOrApproachQuantityOrNameExist(AnaerobicExerciseEntity entity) {
//        List<AnaerobicExerciseEntity> foundAnaerobicExercises = anaerobicExerciseRepository.findByUserEntityOrApproachQuantityOrName(entity.getUser(),
//                entity.getApproachQuantity(),
//                entity.getName());
//
//        foundAnaerobicExercises.stream()
//                .filter(anaerobicExerciseEntity -> !anaerobicExerciseEntity.getUser()
//                        .equals(anaerobicExerciseEntity.getApproachQuantity()
//                                .equals(anaerobicExerciseEntity.getName())))
//                .forEach(anaerobicExerciseEntity -> {
//                    throw new AnaerobicExerciseNotNullFieldExeption("User:" + entity.getUser()
//                            + "ApproachQuantity:" + entity.getApproachQuantity()
//                            + "Name:" + entity.getName());
//                });
//    }
}
