package com.example.fitnessmanager.service.impl;

import com.example.fitnessmanager.dto.food.FoodCreateDto;
import com.example.fitnessmanager.dto.food.FoodFullDto;
import com.example.fitnessmanager.dto.food.FoodUpdateDto;
import com.example.fitnessmanager.entity.DayReportEntity;
import com.example.fitnessmanager.entity.FoodEntity;
import com.example.fitnessmanager.entity.UserEntity;
import com.example.fitnessmanager.exeption.*;
import com.example.fitnessmanager.repository.DayReportRepository;
import com.example.fitnessmanager.repository.FoodRepository;
import com.example.fitnessmanager.repository.UserRepository;
import com.example.fitnessmanager.service.FoodService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.example.fitnessmanager.mapper.FoodMapper.*;

@Slf4j
@Service
public class FoodServiceImpl implements FoodService {

    @Autowired
    private FoodRepository foodRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DayReportRepository dayReportRepository;

    @Override
    @Transactional(readOnly = true)
    public FoodFullDto findById(Long id) {
        FoodEntity foundEntity = foodRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        log.info("FoodServiceImpl -> foud FoodEntity {} by id: {}", foundEntity, id);

        return FOOD_MAPPER.mapToFullDto(foundEntity);
    }

    @Override
    @Transactional(readOnly = true)
    public List<FoodFullDto> findAll() {
        List<FoodEntity> foundEntities = foodRepository.findAll();

        log.info("FoodServiceImpl -> found {} FoodEntity", foundEntities.size());

        return FOOD_MAPPER.toFullDtoList(foundEntities);
    }

    @Override
    @Transactional
    public FoodFullDto create(FoodCreateDto foodCreateDto) {
        FoodEntity entityToSave = FOOD_MAPPER.mapToEntity(foodCreateDto);

        UserEntity userEntity = userRepository.findById(foodCreateDto.getUserId())
                .orElseThrow(() -> new EntityNotFoundExeption(entityToSave.getId()));

        DayReportEntity dayReportEntity = dayReportRepository.findById(foodCreateDto.getDayReportId())
                .orElseThrow(() -> new EntityNotFoundExeption(entityToSave.getId()));

//        checkIfNameOrFatsOrProteinIsTakenExist(entityToSave);

        entityToSave.setUser(userEntity);
        entityToSave.setDayReport(dayReportEntity);

        FoodEntity savedEntity = foodRepository.save(entityToSave);

        log.info("FoodServiceImpl -> FoodEntity {} successfully saved", entityToSave);

        return FOOD_MAPPER.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public FoodFullDto update(FoodUpdateDto foodUpdateDto) {
        FoodEntity entityToUpdate = foodRepository.findById(foodUpdateDto.getId())
                .orElseThrow(() -> new EntityNotFoundExeption(foodUpdateDto.getId()));

        entityToUpdate.setCarbohydrates(foodUpdateDto.getCarbohydrates());

        FoodEntity updatedEntity = foodRepository.save(entityToUpdate);

        log.info("FoodServiceImpl -> FoodEntity {} successfully updated", updatedEntity);

        return FOOD_MAPPER.mapToFullDto(updatedEntity);
    }

    @Override
    public void deleteById(Long id) {
        foodRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundExeption(id));

        foodRepository.deleteById(id);

        log.info("FoodServiceImpl -> FoodEntity {} successfully deleted", id);
    }

//    @Transactional
//    public void checkIfNameOrFatsOrProteinIsTakenExist(FoodEntity entity) {
//        List<FoodEntity> foundFoods = foodRepository.findByNameOrFatsOrProteins(entity.getName(), entity.getFats(), entity.getProteins());
//
//        foundFoods.stream()
//                .filter(foodEntity -> !foodEntity.getName().equals(foodEntity.getFats().equals(foodEntity.getProteins())))
//                .forEach(foodEntity -> {
//                    throw new FoodNotNullFieldExeption(
//                            "Name:" + entity.getName() + "Fats:" + entity.getFats() + "Proteins:" + entity.getProteins());
//                });
//    }
}
