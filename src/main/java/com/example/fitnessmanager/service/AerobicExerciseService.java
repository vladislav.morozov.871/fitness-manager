package com.example.fitnessmanager.service;

import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseCreateDto;
import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseFullDto;
import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AerobicExerciseService {

    AerobicExerciseFullDto findById(Long id);

    List<AerobicExerciseFullDto> findAll();

    AerobicExerciseFullDto create(AerobicExerciseCreateDto aerobicExerciseCreateDto);

    AerobicExerciseFullDto update(AerobicExerciseUpdateDto aerobicExerciseUpdateDto);

    void deleteById(Long id);
}
