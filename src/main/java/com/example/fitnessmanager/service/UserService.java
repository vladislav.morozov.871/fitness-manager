package com.example.fitnessmanager.service;

import com.example.fitnessmanager.dto.user.UserCreateDto;
import com.example.fitnessmanager.dto.user.UserFullDto;
import com.example.fitnessmanager.dto.user.UserUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    UserFullDto findById(Long id);

    List<UserFullDto> findAll();

    UserFullDto create(UserCreateDto userCreateDto);

    UserFullDto update(UserUpdateDto userUpdateDto);

    void deleteById(Long id);
}
