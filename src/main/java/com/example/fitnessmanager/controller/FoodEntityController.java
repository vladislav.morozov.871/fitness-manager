package com.example.fitnessmanager.controller;

import com.example.fitnessmanager.dto.food.FoodCreateDto;
import com.example.fitnessmanager.dto.food.FoodFullDto;
import com.example.fitnessmanager.dto.food.FoodUpdateDto;
import com.example.fitnessmanager.service.FoodService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(description = "Controller dedicated to manage food")
public class FoodEntityController {

    @Autowired
    private FoodService foodService;

    @GetMapping("/foods/{id}")
    @ApiOperation(value = "Find one food by id", notes = "Existing id must be specified")
    public FoodFullDto getById(@PathVariable Long id) {
        return foodService.findById(id);
    }

    @GetMapping("/foods")
    @ApiOperation(value = "Find all food")
    public List<FoodFullDto> findAll() {
        return foodService.findAll();
    }

    @PostMapping("/foods")
    @ApiOperation(value = "Create food")
    public FoodFullDto create(@RequestBody FoodCreateDto createDto) {
        return foodService.create(createDto);
    }

    @PutMapping("/foods")
    @ApiOperation(value = "Update food")
    public FoodFullDto update(@RequestBody FoodUpdateDto updateDto) {
        return foodService.update(updateDto);
    }

    @DeleteMapping("/foods/{id}")
    @ApiOperation(value = "Delete food by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Long id) {
        foodService.deleteById(id);
    }
}
