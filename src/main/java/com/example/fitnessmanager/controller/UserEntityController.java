package com.example.fitnessmanager.controller;

import com.example.fitnessmanager.dto.user.UserCreateDto;
import com.example.fitnessmanager.dto.user.UserFullDto;
import com.example.fitnessmanager.dto.user.UserUpdateDto;
import com.example.fitnessmanager.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "Controller dedicated to manage user")
@RestController
public class UserEntityController {

    @Autowired
    private UserService userService;

    @GetMapping("/users/{id}")
    @ApiOperation(value = "Find one user by id", notes = "Existing id must be specified")
    public UserFullDto getById(@PathVariable Long id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    @ApiOperation(value = "Find all user")
    public List<UserFullDto> findAll() {
        return userService.findAll();
    }

    @PostMapping("/users")
    @ApiOperation(value = "Create user")
    public UserFullDto create(@RequestBody UserCreateDto createDto) {
        return userService.create(createDto);
    }

    @PutMapping("/users")
    @ApiOperation(value = "Update user")
    public UserFullDto update(@RequestBody UserUpdateDto updateDto) {
        return userService.update(updateDto);
    }

    @DeleteMapping("/users/{id}")
    @ApiOperation(value = "Delete user by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Long id) {
        userService.deleteById(id);
    }
}
