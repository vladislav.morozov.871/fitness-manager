package com.example.fitnessmanager.controller;

import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseCreateDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseFullDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseUpdateDto;
import com.example.fitnessmanager.service.AnaerobicExerciseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(description = "Controller dedicated to manage anaerobicExercise")
public class AnaerobicExerciseController {

    @Autowired
    private AnaerobicExerciseService anaerobicExerciseService;

    @GetMapping("/anaerobicExercises/{id}")
    @ApiOperation(value = "Find one anaerobicExercises by id", notes = "Existing id must be specified")
    public AnaerobicExerciseFullDto getById(@PathVariable Long id) {
        return anaerobicExerciseService.findById(id);
    }

    @GetMapping("/anaerobicExercises")
    @ApiOperation(value = "Find all anaerobicExercises")
    public List<AnaerobicExerciseFullDto> findAll() {
        return anaerobicExerciseService.findAll();
    }

    @PostMapping("/anaerobicExercises")
    @ApiOperation(value = "Create anaerobicExercises")
    public AnaerobicExerciseFullDto create(@RequestBody AnaerobicExerciseCreateDto createDto) {
        return anaerobicExerciseService.create(createDto);
    }

    @PutMapping("/anaerobicExercises")
    @ApiOperation(value = "Update anaerobicExercises")
    public AnaerobicExerciseFullDto update(@RequestBody AnaerobicExerciseUpdateDto updateDto) {
        return anaerobicExerciseService.update(updateDto);
    }

    @DeleteMapping("/anaerobicExercises/{id}")
    @ApiOperation(value = "Delete anaerobicExercises by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Long id) {
        anaerobicExerciseService.deleteById(id);
    }
}
