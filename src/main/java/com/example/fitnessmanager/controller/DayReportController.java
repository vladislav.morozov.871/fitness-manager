package com.example.fitnessmanager.controller;

import com.example.fitnessmanager.dto.dayReport.DayReportFullDto;
import com.example.fitnessmanager.service.DayReportService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(description = "Controller dedicated to manage dayReport")
public class DayReportController {

    @Autowired
    private DayReportService dayReportService;

    @GetMapping("/dayReports/{id}")
    @ApiOperation(value = "Find one dayReport by id", notes = "Existing id must be specified")
    public DayReportFullDto getById(@PathVariable Long id) {
        return dayReportService.findById(id);
    }

    @GetMapping("/dayReports")
    @ApiOperation(value = "Find all dayReport")
    public List<DayReportFullDto> findAll() {
        return dayReportService.findAll();
    }

    @DeleteMapping("/dayReports/{id}")
    @ApiOperation(value = "Delete one dayReport by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Long id) {
        dayReportService.deleteById(id);
    }

}
