package com.example.fitnessmanager.controller;

import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseCreateDto;
import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseFullDto;
import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseUpdateDto;
import com.example.fitnessmanager.service.AerobicExerciseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api(description = "Controller dedicated to manage aerobicExercise")
public class AerobicExerciseController {

    @Autowired
    private AerobicExerciseService aerobicExerciseService;

    @GetMapping("/aerobicExercises/{id}")
    @ApiOperation(value = "Find one aerobicExercises by id", notes = "Existing id must be specified")
    public AerobicExerciseFullDto getById(@PathVariable Long id) {
        return aerobicExerciseService.findById(id);
    }

    @GetMapping("/aerobicExercises")
    @ApiOperation(value = "Find all aerobicExercises")
    public List<AerobicExerciseFullDto> findAll() {
        return aerobicExerciseService.findAll();
    }

    @PostMapping("/aerobicExercises")
    @ApiOperation(value = "Create aerobicExercises")
    public AerobicExerciseFullDto create(@RequestBody AerobicExerciseCreateDto createDto) {
        return aerobicExerciseService.create(createDto);
    }

    @PutMapping("/aerobicExercises")
    @ApiOperation(value = "Update aerobicExercises")
    public AerobicExerciseFullDto update(@RequestBody AerobicExerciseUpdateDto updateDto) {
        return aerobicExerciseService.update(updateDto);
    }

    @DeleteMapping("/aerobicExercises/{id}")
    @ApiOperation(value = "Delete aerobicExercises by id", notes = "Existing id must be specified")
    public void delete(@PathVariable Long id) {
        aerobicExerciseService.deleteById(id);
    }
}
