package com.example.fitnessmanager.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "aerobicExercise")
public class AerobicExerciseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "leadTime", nullable = false)
    private LocalDateTime leadTime;

    @Column(name = "caloriesPerMinute", nullable = false)
    private Integer caloriesPerMinute;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "dayReport_id")
    private DayReportEntity dayReport;
}
