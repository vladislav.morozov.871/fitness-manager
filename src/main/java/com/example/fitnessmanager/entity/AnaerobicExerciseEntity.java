package com.example.fitnessmanager.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "anaerobicExercise")
public class AnaerobicExerciseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "approachQuantity", nullable = false)
    private Integer approachQuantity;

    @Column(name = "repetitionsApproachQuantity", nullable = false)
    private Integer repetitionsApproachQuantity;

    @Column(name = "caloriesApproach", nullable = false)
    private Integer caloriesApproach;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @ManyToOne
    @JoinColumn(name = "dayReport_id")
    private DayReportEntity dayReport;
}
