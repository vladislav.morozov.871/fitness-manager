package com.example.fitnessmanager.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "dayReport")
public class DayReportEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sumConsumedCalories", nullable = false)
    private Integer sumConsumedCalories;

    @Column(name = "sumBurnedCalories", nullable = false)
    private Integer sumBurnedCalories;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @OneToMany(mappedBy = "dayReport", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private List<AerobicExerciseEntity> aerobicExercises = new ArrayList<>();

    @OneToMany(mappedBy = "dayReport", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private List<AnaerobicExerciseEntity> anaerobicExercises = new ArrayList<>();

    @OneToMany(mappedBy = "dayReport", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private List<FoodEntity> foods = new ArrayList<>();
}
