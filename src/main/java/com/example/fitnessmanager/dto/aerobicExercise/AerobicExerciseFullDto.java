package com.example.fitnessmanager.dto.aerobicExercise;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AerobicExerciseFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current aerobicExercise")
    private Long id;

    @ApiModelProperty(notes = "AerobicExercise name")
    private String name;

    @ApiModelProperty(example = "18:58", notes = "LeadTime for aerobicExercise")
    private LocalDateTime leadTime;

    @ApiModelProperty(example = "80cl", notes = "CaloriesPerMinute for aerobicExercise")
    private Integer caloriesPerMinute;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Long userId;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Long dayReportId;
}
