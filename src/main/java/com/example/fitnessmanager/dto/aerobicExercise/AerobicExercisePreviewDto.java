package com.example.fitnessmanager.dto.aerobicExercise;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AerobicExercisePreviewDto {

    @ApiModelProperty(example = "1", notes = "Id of the current aerobicExercise")
    private Long id; // id обновляемого уровня

    @ApiModelProperty(notes = "AerobicExercise name")
    private String name;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer dayReportId;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;
}
