package com.example.fitnessmanager.dto.dayReport;

import com.example.fitnessmanager.dto.aerobicExercise.AerobicExercisePreviewDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExercisePreviewDto;
import com.example.fitnessmanager.dto.food.FoodPreviewDto;
import com.example.fitnessmanager.dto.user.UserPreviewDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DayReportFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current dayReport")
    private Long id;

    @ApiModelProperty(example = "500cl" , notes = "SumConsumedCalories for dayReport")
    private Integer sumConsumedCalories;

    @ApiModelProperty(example = "300cl" , notes = "SumBurnedCalories for dayReport")
    private Integer sumBurnedCalories;

    @ApiModelProperty(notes = "List of users for dayReport")
    private List<UserPreviewDto> users;

    @ApiModelProperty(notes = "List of aerobicExercises for dayReport")
    private List<AerobicExercisePreviewDto> aerobicExercises;

    @ApiModelProperty(notes = "List of anaerobicExercises for dayReport")
    private List<AnaerobicExercisePreviewDto> anaerobicExercises;

    @ApiModelProperty(notes = "List of food for dayReport")
    private List<FoodPreviewDto> food;
}
