package com.example.fitnessmanager.dto.dayReport;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DayReportPreviewDto {

    @ApiModelProperty(example = "1", notes = "Id of the current dayReport")
    private Long id; // id обновляемого уровня

    @ApiModelProperty(example = "500cl", notes = "SumConsumedCalories for dayReport")
    private Integer sumConsumedCalories;

    @ApiModelProperty(example = "300cl", notes = "SumBurnedCalories for dayReport")
    private Integer sumBurnedCalories;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;
}
