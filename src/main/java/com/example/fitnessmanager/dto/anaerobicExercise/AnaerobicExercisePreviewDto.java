package com.example.fitnessmanager.dto.anaerobicExercise;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaerobicExercisePreviewDto {

    @ApiModelProperty(example = "1", notes = "Id of the current anaerobicExercise")
    private Long id; // id обновляемого уровня

    @ApiModelProperty(notes = "AnaerobicExercise name")
    private String name;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer dayReportId;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Integer userId;
}
