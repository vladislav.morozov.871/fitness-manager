package com.example.fitnessmanager.dto.anaerobicExercise;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaerobicExerciseUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current anaerobicExercise")
    private Long id; // id обновляемого уровня

    @ApiModelProperty(notes = "AnaerobicExercise name")
    private String name;

    @ApiModelProperty(example = "29", notes = "ApproachQuantity for anaerobicExercise")
    private Integer approachQuantity;

    @ApiModelProperty(example = "3", notes = "RepetitionsApproachQuantity for anaerobicExercise")
    private Integer repetitionsApproachQuantity;

    @ApiModelProperty(example = "300", notes = "CaloriesApproach for anaerobicExercise")
    private Integer caloriesApproach;
}
