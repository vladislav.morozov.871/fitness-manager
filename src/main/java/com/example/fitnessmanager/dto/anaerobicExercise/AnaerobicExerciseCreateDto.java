package com.example.fitnessmanager.dto.anaerobicExercise;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AnaerobicExerciseCreateDto {

    @ApiModelProperty(notes = "AnaerobicExercise name")
    private String name;

    @ApiModelProperty(example = "29", notes = "ApproachQuantity for anaerobicExercise")
    private Integer approachQuantity;

    @ApiModelProperty(example = "3", notes = "RepetitionsApproachQuantity for anaerobicExercise")
    private Integer repetitionsApproachQuantity;

    @ApiModelProperty(example = "300", notes = "CaloriesApproach for anaerobicExercise")
    private Integer caloriesApproach;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Long userId;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Long dayReportId;
}
