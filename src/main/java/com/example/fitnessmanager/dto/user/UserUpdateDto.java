package com.example.fitnessmanager.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current user")
    private Long id; // id обновляемого уровня

    @ApiModelProperty(example = "20", notes = "User age")
    private Integer age;

    @ApiModelProperty(example = "80 kg", notes = "User weight")
    private Integer weight;

    @ApiModelProperty(example = "180 sm", notes = "User height")
    private Integer height;

    @ApiModelProperty(example = "user@gmail.com", notes = "User email")
    private String email;

    @ApiModelProperty(example = "Nikitos228", notes = "UserName")
    private String userName;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link tp the image file")
    private String imgUrl;
}
