package com.example.fitnessmanager.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsetLoginForEmail {

    @ApiModelProperty(example = "user@gmail.com", notes = "User email")
    private String email;
}
