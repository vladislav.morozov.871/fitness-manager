package com.example.fitnessmanager.dto.user;

import com.example.fitnessmanager.dto.aerobicExercise.AerobicExercisePreviewDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExercisePreviewDto;
import com.example.fitnessmanager.dto.dayReport.DayReportPreviewDto;
import com.example.fitnessmanager.dto.food.FoodPreviewDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserFullDto {

    @ApiModelProperty(example = "1", notes = "Id of the current user")
    private Long id;

    @ApiModelProperty(notes = "User name")
    private String name;

    @ApiModelProperty("User surname")
    private String surname;

    @ApiModelProperty(example = "user@gmail.com", notes = "User email")
    private String email;

    @ApiModelProperty(example = "20", notes = "User age")
    private Integer age;

    @ApiModelProperty(example = "80 kg", notes = "User weight")
    private Integer weight;

    @ApiModelProperty(example = "180 sm", notes = "User height")
    private Integer height;

    @ApiModelProperty(example = "male", notes = "User sex")
    private String sex;

    @ApiModelProperty(example = "Nikitos228", notes = "UserName")
    private String userName;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link tp the image file")
    private String imgUrl;

    @ApiModelProperty(notes = "List of AerobicExercise for user")
    private List<AerobicExercisePreviewDto> aerobicExercisePreviewDtos;

    @ApiModelProperty(notes = "List of AnaerobicExercise for user")
    private List<AnaerobicExercisePreviewDto> anaerobicExercisePreviewDtos;

    @ApiModelProperty(notes = "List of Food for user")
    private List<FoodPreviewDto> foodPreviewDtos;

    @ApiModelProperty(notes = "List of DayReport for user")
    private List<DayReportPreviewDto> dayReportPreviewDtos;
}
