package com.example.fitnessmanager.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserCreateDto {

    @ApiModelProperty(notes = "User name")
    private String name;

    @ApiModelProperty("User surname")
    private String surname;

    @ApiModelProperty(example = "user@gmail.com", notes = "User email")
    private String email;

    @ApiModelProperty(example = "nik124" , notes = "User password")
    private String password;

    @ApiModelProperty(example = "20", notes = "User age")
    private Integer age;

    @ApiModelProperty(example = "80 ", notes = "User weight")
    private Integer weight;

    @ApiModelProperty(example = "180", notes = "User height")
    private Integer height;

    @ApiModelProperty(example = "male", notes = "User sex")
    private String sex;

    @ApiModelProperty(example = "nikita99" )
    private String userName;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link tp the image file")
    private String imgUrl;
}
