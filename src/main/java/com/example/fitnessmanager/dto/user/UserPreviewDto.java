package com.example.fitnessmanager.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserPreviewDto {

    @ApiModelProperty(example = "1", notes = "Id of the current user")
    private Long id; // id обновляемого уровня

    @ApiModelProperty(example = "Nikitos228", notes = "UserName")
    private String userName;

    @ApiModelProperty(example = "http://image.jpg", notes = "Link tp the image file")
    private String imgUrl;

}
