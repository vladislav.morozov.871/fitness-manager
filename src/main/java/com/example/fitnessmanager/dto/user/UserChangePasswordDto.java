package com.example.fitnessmanager.dto.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserChangePasswordDto {

    @ApiModelProperty(example = "nik124", notes = "User password")
    private String password;

    @ApiModelProperty(example = "nik124", notes = "User old password")
    private String oldPassword;

    @ApiModelProperty(example = "nik123456", notes = "User new password")
    private String newPassword;
}
