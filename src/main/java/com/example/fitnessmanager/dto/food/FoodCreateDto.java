package com.example.fitnessmanager.dto.food;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodCreateDto {

    @ApiModelProperty(example = "sushi", notes = "Food name")
    private String name;

    @ApiModelProperty(example = "30" , notes = "Food proteins")
    private Integer proteins;

    @ApiModelProperty(example = "60" , notes = "Food fats")
    private Integer fats;

    @ApiModelProperty(example = "100" , notes = "Food carbohydrates")
    private Integer carbohydrates;

    @ApiModelProperty(example = "1" , notes = "Existing id must be specified")
    private Long userId;

    @ApiModelProperty(example = "1" , notes = "Existing id must be specified")
    private Long dayReportId;
}
