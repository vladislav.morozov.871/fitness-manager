package com.example.fitnessmanager.dto.food;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodPreviewDto {

    @ApiModelProperty(example = "1", notes = "Id of the current food")
    private Long id; // id обновляемого уровня

    @ApiModelProperty(example = "sushi", notes = "Food name")
    private String name;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Long userId;

    @ApiModelProperty(example = "1", notes = "Existing id must be specified")
    private Long dayReportId;

}
