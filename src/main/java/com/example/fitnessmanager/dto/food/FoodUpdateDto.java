package com.example.fitnessmanager.dto.food;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FoodUpdateDto {

    @ApiModelProperty(example = "1", notes = "Id of the current food")
    private Long id;

    @ApiModelProperty(example = "30", notes = "Food proteins")
    private Integer proteins;

    @ApiModelProperty(example = "60", notes = "Food fats")
    private Integer fats;

    @ApiModelProperty(example = "100", notes = "Food carbohydrates")
    private Integer carbohydrates;
}
