package com.example.fitnessmanager.mapper;

import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseCreateDto;
import com.example.fitnessmanager.dto.anaerobicExercise.AnaerobicExerciseFullDto;
import com.example.fitnessmanager.entity.AnaerobicExerciseEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AnaerobicExerciseMapper {

    AnaerobicExerciseMapper ANAEROBIC_EXERCISE_MAPPER = Mappers.getMapper(AnaerobicExerciseMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "dayReport", ignore = true)
    AnaerobicExerciseEntity mapToEntity(AnaerobicExerciseCreateDto createDto);

    AnaerobicExerciseFullDto mapToFullDto(AnaerobicExerciseEntity anaerobicExerciseEntity);

    List<AnaerobicExerciseFullDto> toFullDtoList(List<AnaerobicExerciseEntity> entities);
}
