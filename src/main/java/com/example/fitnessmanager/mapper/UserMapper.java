package com.example.fitnessmanager.mapper;

import com.example.fitnessmanager.dto.user.UserCreateDto;
import com.example.fitnessmanager.dto.user.UserFullDto;
import com.example.fitnessmanager.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "aerobicExercises", ignore = true)
    @Mapping(target = "anaerobicExercises", ignore = true)
    @Mapping(target = "foods", ignore = true)
    @Mapping(target = "dayReports", ignore = true)
    UserEntity mapToEntity(UserCreateDto createDto);

    UserFullDto mapToFullDto(UserEntity userEntity);

    List<UserFullDto> toFullDtoList(List<UserEntity> entities);
}
