package com.example.fitnessmanager.mapper;

import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseCreateDto;
import com.example.fitnessmanager.dto.aerobicExercise.AerobicExerciseFullDto;
import com.example.fitnessmanager.entity.AerobicExerciseEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AerobicExerciseMapper {

    AerobicExerciseMapper AEROBIC_EXERCISE_MAPPER = Mappers.getMapper(AerobicExerciseMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "dayReport", ignore = true)
    AerobicExerciseEntity mapToEntity(AerobicExerciseCreateDto createDto);

    AerobicExerciseFullDto mapToFullDto(AerobicExerciseEntity aerobicExerciseEntity);

    List<AerobicExerciseFullDto> toFullDtoList(List<AerobicExerciseEntity> entities);
}
