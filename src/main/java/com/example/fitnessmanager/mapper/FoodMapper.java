package com.example.fitnessmanager.mapper;

import com.example.fitnessmanager.dto.food.FoodCreateDto;
import com.example.fitnessmanager.dto.food.FoodFullDto;
import com.example.fitnessmanager.entity.FoodEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FoodMapper {

    FoodMapper FOOD_MAPPER = Mappers.getMapper(FoodMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "dayReport", ignore = true)
    FoodEntity mapToEntity(FoodCreateDto createDto);

    FoodFullDto mapToFullDto(FoodEntity foodEntity);

    List<FoodFullDto> toFullDtoList(List<FoodEntity> entities);
}
