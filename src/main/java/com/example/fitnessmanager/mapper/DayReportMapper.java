package com.example.fitnessmanager.mapper;

import com.example.fitnessmanager.dto.dayReport.DayReportCreateDto;
import com.example.fitnessmanager.dto.dayReport.DayReportFullDto;
import com.example.fitnessmanager.entity.DayReportEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DayReportMapper {

    DayReportMapper DAY_REPORT_MAPPER = Mappers.getMapper(DayReportMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "aerobicExercises", ignore = true)
    @Mapping(target = "anaerobicExercises", ignore = true)
    @Mapping(target = "foods", ignore = true)
    DayReportEntity mapToEntity(DayReportCreateDto dayReportCreateDto);

    DayReportFullDto mapToFullDto(DayReportEntity dayReportEntity);

    List<DayReportFullDto> toFullDtoList(List<DayReportEntity> entities);
}
