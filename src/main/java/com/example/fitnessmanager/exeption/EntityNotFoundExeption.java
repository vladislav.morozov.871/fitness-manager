package com.example.fitnessmanager.exeption;

public class EntityNotFoundExeption extends RuntimeException {

    public EntityNotFoundExeption(Long id) {
        super("was not found by id " + id);
    }
}
