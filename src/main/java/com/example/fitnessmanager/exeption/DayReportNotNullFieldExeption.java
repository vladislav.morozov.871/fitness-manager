package com.example.fitnessmanager.exeption;

public class DayReportNotNullFieldExeption extends RuntimeException{

    public DayReportNotNullFieldExeption(String message){
        super(message);
    }
}
