package com.example.fitnessmanager.exeption;

public class AerobicExerciseNotNullFieldExeption extends RuntimeException {

    public AerobicExerciseNotNullFieldExeption(String message) {
        super("Field not found");
    }
}
