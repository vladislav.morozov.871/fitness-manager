package com.example.fitnessmanager.exeption;

public class AnaerobicExerciseNotNullFieldExeption extends RuntimeException {

    public AnaerobicExerciseNotNullFieldExeption(String message) {
        super(message);
    }
}
