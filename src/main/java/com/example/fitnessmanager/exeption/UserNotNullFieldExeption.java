package com.example.fitnessmanager.exeption;

public class UserNotNullFieldExeption extends RuntimeException {

    public UserNotNullFieldExeption(String message) {
        super(message);
    }
}
