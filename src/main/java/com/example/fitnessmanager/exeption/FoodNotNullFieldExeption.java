package com.example.fitnessmanager.exeption;

public class FoodNotNullFieldExeption extends RuntimeException{

    public FoodNotNullFieldExeption(String message){
        super(message);
    }
}
