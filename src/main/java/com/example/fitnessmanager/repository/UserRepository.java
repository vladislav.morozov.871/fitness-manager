package com.example.fitnessmanager.repository;

import com.example.fitnessmanager.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findOneById(Long id);

//    List<UserEntity> findByUserNameOrEmail(String login, String email);
}
