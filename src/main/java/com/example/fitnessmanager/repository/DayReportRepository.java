package com.example.fitnessmanager.repository;

import com.example.fitnessmanager.entity.DayReportEntity;
import com.example.fitnessmanager.entity.FoodEntity;
import com.example.fitnessmanager.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DayReportRepository extends JpaRepository<DayReportEntity, Long> {

    DayReportEntity findOneById(Long id);

//    List<DayReportEntity> findByUserEntityOrSumConsumedCaloriesOrSumBurnedCalories(UserEntity entity, Integer sumConsumedCalories, Integer sumBurnedCalories);
}
