package com.example.fitnessmanager.repository;

import com.example.fitnessmanager.entity.FoodEntity;
import com.example.fitnessmanager.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodRepository extends JpaRepository<FoodEntity, Long> {

    FoodEntity findOneById(Long id);

//    List<FoodEntity> findByNameOrFatsOrProteins(String name, Integer fats, Integer proteins);
}
