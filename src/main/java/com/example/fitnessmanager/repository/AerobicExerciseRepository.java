package com.example.fitnessmanager.repository;

import com.example.fitnessmanager.entity.AerobicExerciseEntity;
import com.example.fitnessmanager.entity.AnaerobicExerciseEntity;
import com.example.fitnessmanager.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AerobicExerciseRepository extends JpaRepository<AerobicExerciseEntity, Long> {

    AerobicExerciseEntity findOneById(Long id);

//    List<AerobicExerciseEntity> findByUserEntityOrCaloriesPerMinuteOrName(UserEntity entity, Integer caloriesPerMinute, String name);
}
