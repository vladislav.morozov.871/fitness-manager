package com.example.fitnessmanager.repository;

import com.example.fitnessmanager.entity.AnaerobicExerciseEntity;
import com.example.fitnessmanager.entity.DayReportEntity;
import com.example.fitnessmanager.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnaerobicExerciseRepository extends JpaRepository<AnaerobicExerciseEntity, Long> {

    AnaerobicExerciseEntity findOneById(Long id);

//    List<AnaerobicExerciseEntity> findByUserEntityOrApproachQuantityOrName(UserEntity entity, Integer approachQuantity, String name);

}
